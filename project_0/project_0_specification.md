# Project 0: Simple Dungeon

## Description

Your first project is a simple dungeon crawler. You are an adventurer seeking treasure and you come across a cave filled with monsters and gold.

You will create a terminal application that prints to standard out and reads from standard in.

## Minimum Requirements

The dungeon is divided into a series of rooms. In each room, players will be presented with some text describing the room, a numbered list of actions to take, and a prompt. Players will select an action by entering the list item number.

~~~
Room description

1) first option
2) second option
>
~~~

Each room description (other than the first) will start with a line break to separate it from the text in the previous room.

Your program will read a `String` from standard input and attempt to convert it into an `int`. If the `String` cannot be converted to an `int`, or the value is not a valid choice, an appropriate error message will be printed and the program will exit.

If the user inputs something that cannot be converted to `int`, your program will print

`Invalid input: '<input>' is not an integer`

If the user inputs an integer that is out of range, your program will print

`Invalid choice: '<input>' is out of range`

### Room Description

Here is a map of the dungeon.

<img src="map.svg" alt="drawing" width="200"/>

Each room is given an ID. The following is a description of the contents of each room.

#### Room 0

~~~
You come across a cave in the woods and decide to enter.
Inside there are some weapons. Which will you choose?

1) Sword
2) Magic Staff
~~~

1) Print

```
You pick up the sword and tuck it into your belt,
then proceed deeper into the cave.
```

Remember that the player chose a sword and proceed to Room 1.

2) Print

```
You grab the magic staff and use it as a walking stick
as you proceed deeper into the cave.
```
Remember that the player chose a magic staff and proceed to Room 1.

#### Room 1

~~~
You crawl down a pit and drop into a room dimly lit by torches.
There are two exits at the far end of the room,
the left one is guarded by goblins, but the right one is exposed.
There is also a small treasure chest beside you.

1) Fight the goblins on the left
2) Sneak past the goblins into the exit on the right
3) Take the treasure and leave
~~~

1) Print

```
You easily kill the goblins with your <weapon> and proceed.
```

(Make sure to replace `<weapon>` with what the player actually chose.) The player continues to Room 2.

2) Print

```
You manage to sneak by without raising suspicion.
```

The player continues to Room 3.

3) Print

```
You make it out with your modest loot,
unscathed but disappointed you couldn't get more.
```

The game will exit.

#### Room 2

~~~
Even more goblins await you on the other side of the passage.
They immediately notice you!

1) Use your knowledge
2) Use your strength
3) Try to reason with them
4) Run
~~~

1) If the player wields a magic staff, print

```
You summon a fireball with your staff and burn the goblins to bits!
```

and continue to Room 4. If the player wields a sword print

```
You try to fight with a fancy form,
but the goblins don't care and overwhelm you!
```

and exit.

2) If the player wields a sword, print

~~~
You charge at the mob spinning your sword swiftly
and tearing through the goblins.
~~~

and continue to Room 4. If the player wields a magic staff, print

~~~
You swing your staff as hard as you can, killing the nearest goblin,
but the mob quickly dog-piles you.
~~~

and exit.

3) Print

~~~
You hold out your hands in peace and say,
'Hold on guys, I think there's a mutually beneficial solution-'
you are cut off by the goblins grunting and they rush you.
They catch you unprepared and defeat you.
~~~

and exit.

4) Print

~~~
You flee in fear with goblins close behind.
You escape, but you are ashamed that your exploration was fruitless.
~~~

and exit.

#### Room 4

~~~
The next room is well-lit with many torches.
You see a pedestal in the center of the room. Two magical amulets lay on top.
Which will you choose?

1) The triangle-shaped amulet
2) The star-shaped amulet
~~~

1) Print

~~~
You pick up the triangle-shaped amulet. The pedestal immediately retracts into
the floor. You don the amulet and push open the ornate door
leading to the next room.
~~~

Continue to Room 6.

2) Print

~~~
You pick up the star-shaped amulet. The pedestal immediately retracts into
the floor. You don the amulet and push open the ornate door
leading to the next room. the amulet and push open the ornate door leading to the next room.
~~~

Continue to Room 6.

#### Room 3

~~~
The next room is empty and cold. You carefully walk to the end and find a sturdy
gate blocking your way with no apparent way to open it.
Suddenly a disembodied voice whispers in your ear.

'I am not alive, but I grow; I don't have lungs, but I need air;
I don't have a mouth, but water kills me. What am I?'

You understand that this is a riddle and speaking the answer will open the gate.
What do you say?

>
~~~

The player should enter the answer to the riddle, which is "fire". If the String "fire" appears anywhere in the player's answer, count it correct. This will be done without regard to case, meaning "Fire", "fire", and "FIRE" would all be acceptable answers.

If the player enters the answer correctly, print

~~~
'Fire!' you exclaim. The gate immediately begins to open
and you quickly walk through.
~~~

and continue to Room 5.

If the player does not enter the answer correctly, print 

~~~
'<input>?' you guess.
The voice whispers back, 'Wrong answer...'
For a second, nothing happens, but suddenly everything goes dark.
~~~

and exit.

#### Room 5

~~~
The next room is well-lit with many torches.
You see a pedestal in the center of the room. Two magical amulets lay on top.
Which will you choose?

1) The heart-shaped amulet
2) The eye-shaped amulet
~~~

1) Print

~~~
You pick up the heart-shaped amulet. The pedestal immediately retracts into
the floor. You don the amulet and push open the ornate door
leading to the next room.
~~~

Continue to Room 6.

2) Print

~~~
You pick up the eye-shaped amulet. The pedestal immediately retracts into
the floor. You don the amulet and push open the ornate door
leading to the next room.
~~~

Continue to Room 6.

#### Room 6

Room 6 is a turn-based boss fight. The player has at most three turns to defeat the boss, if after the third turn the boss is not dead, the player loses automatically.

The boss starts with 20hp. The player is invulnerable, the only way to lose is to run out of turns.

In each turn, the player can choose to use their amulet or attack with their weapon. See below for details.

When the player first enters the room, print

~~~
You creep into the room and you hear a low growl. You take a few more cautious
steps and the room is suddenly illuminated with a huge flame.
You see a massive dragon at the other end of the fire.
The torches behind you ignite from the heat,
allowing you to see the dragon's full form.
~~~

At the beginning of each turn, print

~~~
The dragon attacks you, but you are nimble and stay ahead of it.

1) Use amulet
2) Attack
~~~

1) The following effects will be applied based on their amulet.

* Triangle-shaped: If the player wields a sword, raise the critical chance to 75%. Print

~~~
The amulet pulses and imbues power into your sword,
raising the chance of critical hits.
~~~

If the player wields a staff, just print

~~~
The amulet glows with power, but doesn't seem to effect your staff!
~~~

* Star-shaped: If the player wields a magic staff, raise the critical chance to 75%. Print

~~~
The amulet pulses and strengthens your magic staff,
raising the chance of critical hits.
~~~

If the player wields a sword, just print

~~~
The amulet glows with magic, but doesn't seem to effect your sword!
~~~

* Heart-shaped: Automatically reduce the boss by 10hp. Print

~~~
The amulet glows and pulls energy from the dragon into itself.
The dragon lost 10hp!
~~~

* Eye-shaped: Reveal the boss' hp. Print

~~~
The amulet scans the dragon and reveals its hp to you.
The dragon has <hp>hp left.
~~~

2) The player will deal 5hp of damage with a 25% chance of a critical hit, which triples the damage to 15hp. Print

~~~
You attack the dragon and deal <hp>hp of damage!
~~~

Subtract the damage from the boss' health.

At the end of each turn, check if the boss has been defeated. If so, print

~~~
The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!
This will fund your adventuring for years to come!
~~~

then exit.

If the player completes the third turn without killing the boss, print

~~~
Your luck finally runs out as the dragon connects a strike.
You're winded by the massive blow and are unable to dodge
the torrent of flames that fall on you, burning you to a crisp.
~~~

then exit.

### Compiling

You will name your main class `SimpleDungeon`. From your project directory, Your program will compile with `javac SimpleDungeon.java`, and run with `java SimpleDungeon`.

### Report

You will create a report that describes your implementation. You may create your report with any software you like, Microsoft Word, LibreOffice Writer, HTML, etc, but you must export a *PDF* for submission.

Your report will contain the following sections.

* Introduction
    - Describe the project in your own words.

* Implementation
    - Discuss how you managed user input, separating the logic for each room, and fighting the final boss.

* Bugs
    - Describe any bugs you created and how you solved them, if any.

* Extra Credit
    - Document any extra credit features you added.

Your report will be named `<first_name>_report_p0.pdf`.

## Project Policies

### Group Work

You are encouraged to work with your classmates on this project, but you may not share code. It is acceptable to discuss solutions and help debug, but you cannot provide another student with your code.

### Unoriginal Code

You are not permitted to use any code that you didn't write, outside of the following classes from the standard library. (Exceptions may be made for extra credit features.)

* java.util.Scanner

* java.util.Random

### Submission

Submit a zip named `<first_name>_p0.zip` with the following contents.

* `SimpleDungeon.java`
* Any additional source files (unlikely to be needed)
* `<first_name>_report_p0.pdf`

## Reference Implementation

A reference implementation is provided. If you have questions about operation, you can run the reference program like so

~~~
java -jar SimpleDungeon.jar
~~~

This should help clear up any unclear requirements.

You may think it's possible to reverse engineer this application and cheat. You'd be right! But you'll get caught if you try to use this code. Decompiled code is very easy to detect, and it will be obvious from your report if you do not understand what you created.

## Auto Grader

An auto grader can run your program to determine if your program is functionally correct. This represents the "correctness" portion of your grade.

Usage:

~~~
java -jar SimpleDungeonGrader.jar <project_directory>
~~~

The first argument `<project_directory>` should be the path to your project directory which contains your `SimpleDungeon.java` file. The grader will use this directory to build and run your project.

The grader will log what it is doing, giving you a PASS or FAIL for each test and a total at the bottom. After it is finished, you can find a `grade.txt` in the directory you ran the grader from which will expound on any failures uncovered and should help you debug why they failed.

## Extra Credit

For extra credit, you may extend your game with additional features or rooms. If you can do this without breaking the auto-grader, great! If not, email me and I will go over your proposal to approve it, and if approved I will grade it manually.

Some suggestions:

* Add more (non-trivial) options to some rooms

* Add more rooms with different contents.

* Create a different ending message based on the choices the player made.

* Add alternate endings

If you are already an experienced programmer, you may wish to show off with things like

* Allowing the user to re-enter input after causing an error

* Altering the boss fight to include true turn based combat, not the fixed hacky thing we have here.

* Randomly selecting additional actions for some rooms so that each play through has unique options

Be creative! I'm likely to approve anything in the spirit of the original specification.

## Rubric

Your project will be graded according to the following rubric.

|   Category   | Weight |
|:------------:|:------:|
|  Correctness |   70   |
|     Style    |   10   |
|    Report    |   20   |
| Extra Credit |   10   |
