import java.util.Scanner;
import java.util.Random;

public class SimpleDungeon {
    public static void main(String[] args) {
        // Constants
        String SWORD = "sword";
        String STAFF = "magic staff";

        String TRIANGLE_AMULET = "triangle";
        String STAR_AMULET = "star";
        String HEART_AMULET = "heart";
        String EYE_AMULET = "eye";

        // Player input variables
        Scanner inputScanner = new Scanner(System.in);
        int playerChoice;
        String playerInput;

        // --- Room 0 ---
        System.out.println("You come across a cave in the woods and decide to enter.");
        System.out.println("Inside there are some weapons. Which will you choose?");
        System.out.println();
        System.out.println("1) Sword");
        System.out.println("2) Magic Staff");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        String weapon;

        if (playerChoice == 1) {
            System.out.println("You pick up the sword and tuck it into your belt,");
            System.out.println("then proceed deeper into the cave.");
            weapon = SWORD;
        }
        else if (playerChoice == 2) {
            System.out.println("You grab the magic staff and use it as a walking stick");
            System.out.println("as you proceed deeper into the cave.");
            weapon = STAFF;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        // --- Room 1 ---
        System.out.println();
        System.out.println("You crawl down a pit and drop into a room dimly lit by torches.");
        System.out.println("There are two exits at the far end of the room,");
        System.out.println("the left one is guarded by goblins, but the right one is exposed.");
        System.out.println("There is also a small treasure chest beside you.");
        System.out.println();
        System.out.println("1) Fight the goblins on the left");
        System.out.println("2) Sneak past the goblins into the exit on the right");
        System.out.println("3) Take the treasure and leave");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        int nextRoom;

        if (playerChoice == 1) {
            System.out.println("You easily kill the goblins with your " + weapon + " and proceed.");
            nextRoom = 2;
        }
        else if (playerChoice == 2) {
            System.out.println("You manage to sneak by without raising suspicion.");
            nextRoom = 3;
        }
        else if (playerChoice == 3) {
            System.out.println("You make it out with your modest loot,");
            System.out.println("unscathed but disappointed you couldn't get more.");
            return;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        // --- Rooms 2 & 3 ---
        if (nextRoom == 2) {
            System.out.println();
            System.out.println("Even more goblins await you on the other side of the passage.");
            System.out.println("They immediately notice you!");
            System.out.println();
            System.out.println("1) Use your knowledge");
            System.out.println("2) Use your strength");
            System.out.println("3) Try to reason with them");
            System.out.println("4) Run");
            System.out.print("> ");

            try {
                playerChoice = inputScanner.nextInt();
            }
            catch (java.util.InputMismatchException e) {
                playerInput = inputScanner.nextLine();
                System.out.println("Invalid input: '" + playerInput + "' is not an integer");
                return;
            }

            if (playerChoice == 1) {
                if (weapon.equals(STAFF)) {
                    System.out.println("You summon a fireball with your staff and burn the goblins to bits!");
                }
                else {
                    System.out.println("You try to fight with a fancy form,");
                    System.out.println("but the goblins don't care and overwhelm you!");
                    return;
                }
            }
            else if (playerChoice == 2) {
                if (weapon.equals(SWORD)) {
                    System.out.println("You charge at the mob spinning your sword swiftly");
                    System.out.println("and tearing through the goblins.");
                }
                else {
                    System.out.println("You swing your staff as hard as you can, killing the nearest goblin,");
                    System.out.println("but the mob quickly dog-piles you.");
                    return;
                }
            }
            else if (playerChoice == 3) {
                System.out.println("You hold out your hands in peace and say,");
                System.out.println("'Hold on guys, I think there's a mutually beneficial solution-'");
                System.out.println("you are cut off by the goblins grunting and they rush you.");
                System.out.println("They catch you unprepared and defeat you.");
                return;
            }
            else if (playerChoice == 4) {
                System.out.println("You flee in fear with goblins close behind.");
                System.out.println("You escape, but you are ashamed that your exploration was fruitless.");
                return;
            }
            else {
                System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
                return;
            }

            nextRoom = 4;
        }
        else if (nextRoom == 3) {
            System.out.println();
            System.out.println("The next room is empty and cold. You carefully walk to the end and find a sturdy");
            System.out.println("gate blocking your way with no apparent way to open it.");
            System.out.println("Suddenly a disembodied voice whispers in your ear.");
            System.out.println();
            System.out.println("'I am not alive, but I grow; I don't have lungs, but I need air;");
            System.out.println("I don't have a mouth, but water kills me. What am I?'");
            System.out.println();
            System.out.println("You understand that this is a riddle and speaking the answer will open the gate.");
            System.out.println("What do you say?");
            System.out.print("> ");

            // Clear the input, the previous interaction will have left a newline in the buffer
            inputScanner.nextLine();
            playerInput = inputScanner.nextLine();
            if (playerInput.toLowerCase().contains("fire")) {
                System.out.println("'Fire!' you exclaim. The gate immediately begins to open");
                System.out.println("and you quickly walk through.");
            }
            else {
                System.out.println("'" + playerInput + "?' you guess.");
                System.out.println("The voice whispers back, 'Wrong answer...'");
                System.out.println("For a second, nothing happens, but suddenly everything goes dark.");
                return;
            }

            nextRoom = 5;
        }
        else {
            System.out.println("This is a bug");
            return;
        }

        // --- Room 4 & 5 ---
        String amulet;
        String amuletChoice1;
        String amuletChoice2;

        if (nextRoom == 4) {
            amuletChoice1 = TRIANGLE_AMULET;
            amuletChoice2 = STAR_AMULET;
        }
        else if (nextRoom == 5) {
            amuletChoice1 = HEART_AMULET;
            amuletChoice2 = EYE_AMULET;
        }
        else {
            System.out.println("This is a bug");
            return;
        }

        System.out.println();
        System.out.println("The next room is well-lit with many torches.");
        System.out.println("You see a pedestal in the center of the room. Two magical amulets lay on top.");
        System.out.println("Which will you choose?");
        System.out.println();
        System.out.println("1) The " + amuletChoice1 + "-shaped amulet");
        System.out.println("2) The " + amuletChoice2 + "-shaped amulet");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        if (playerChoice == 1) {
            amulet = amuletChoice1;
        }
        else if (playerChoice == 2) {
            amulet = amuletChoice2;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        System.out.println("You pick up the " + amulet + "-shaped amulet. The pedestal immediately retracts into");
        System.out.println("the floor. You don the amulet and push open the ornate door");
        System.out.println("leading to the next room.");

        // --- Room 6 ---
        int bossHp = 20;
        int criticalChance = 25;
        int baseDamage = 5;
        int criticalDamage = baseDamage * 3;

        System.out.println();
        System.out.println("You creep into the room and you hear a low growl. You take a few more cautious");
        System.out.println("steps and the room is suddenly illuminated with a huge flame.");
        System.out.println("You see a massive dragon at the other end of the fire.");
        System.out.println("The torches behind you ignite from the heat,");
        System.out.println("allowing you to see the dragon's full form.");

        // --- Turn 1 ---
        System.out.println();
        System.out.println("The dragon attacks you, but you are nimble and stay ahead of it.");
        System.out.println();
        System.out.println("1) Use amulet");
        System.out.println("2) Attack");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        if (playerChoice == 1) {
            if (amulet.equals(TRIANGLE_AMULET)) {
                if (weapon.equals(SWORD)) {
                    System.out.println("The amulet pulses and imbues power into your sword,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with power, but doesn't seem to effect your staff!");
                }
            }
            else if (amulet.equals(STAR_AMULET)) {
                if (weapon.equals(STAFF)) {
                    System.out.println("The amulet pulses and strengthens your magic staff,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with magic, but doesn't seem to effect your sword!");
                }
            }
            else if (amulet.equals(HEART_AMULET)) {
                System.out.println("The amulet glows and pulls energy from the dragon into itself.");
                System.out.println("The dragon lost 10hp!");
                bossHp = bossHp - 10;
            }
            else if (amulet.equals(EYE_AMULET)) {
                System.out.println("The amulet scans the dragon and reveals its hp to you.");
                System.out.println("The dragon has " + bossHp + "hp left.");
            }
        }
        else if (playerChoice == 2) {
            int damage = baseDamage;
            // Roll a random number on [1, 100]
            Random rng = new Random();
            int roll = rng.nextInt(100) + 1;
            if (roll <= criticalChance) {
                damage = criticalDamage;
            }

            System.out.println("You attack the dragon and deal " + damage + "hp of damage!");
            bossHp = bossHp - damage;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        if (bossHp <= 0) {
            System.out.println();
            System.out.println("The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!");
            System.out.println("This will fund your adventuring for years to come!");
            return;
        }

        // --- Turn 2 ---
        System.out.println();
        System.out.println("The dragon attacks you, but you are nimble and stay ahead of it.");
        System.out.println();
        System.out.println("1) Use amulet");
        System.out.println("2) Attack");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        if (playerChoice == 1) {
            if (amulet.equals(TRIANGLE_AMULET)) {
                if (weapon.equals(SWORD)) {
                    System.out.println("The amulet pulses and imbues power into your sword,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with power, but doesn't seem to effect your staff!");
                }
            }
            else if (amulet.equals(STAR_AMULET)) {
                if (weapon.equals(STAFF)) {
                    System.out.println("The amulet pulses and strengthens your magic staff,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with magic, but doesn't seem to effect your sword!");
                }
            }
            else if (amulet.equals(HEART_AMULET)) {
                System.out.println("The amulet glows and pulls energy from the dragon into itself.");
                System.out.println("The dragon lost 10hp!");
                bossHp = bossHp - 10;
            }
            else if (amulet.equals(EYE_AMULET)) {
                System.out.println("The amulet scans the dragon and reveals its hp to you.");
                System.out.println("The dragon has " + bossHp + "hp left.");
            }
        }
        else if (playerChoice == 2) {
            int damage = baseDamage;
            // Roll a random number on [1, 100]
            Random rng = new Random();
            int roll = rng.nextInt(100) + 1;
            if (roll <= criticalChance) {
                damage = criticalDamage;
            }

            System.out.println("You attack the dragon and deal " + damage + "hp of damage!");
            bossHp = bossHp - damage;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        if (bossHp <= 0) {
            System.out.println();
            System.out.println("The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!");
            System.out.println("This will fund your adventuring for years to come!");
            return;
        }

        // --- Turn 3 ---
        System.out.println();
        System.out.println("The dragon attacks you, but you are nimble and stay ahead of it.");
        System.out.println();
        System.out.println("1) Use amulet");
        System.out.println("2) Attack");
        System.out.print("> ");

        try {
            playerChoice = inputScanner.nextInt();
        }
        catch (java.util.InputMismatchException e) {
            playerInput = inputScanner.nextLine();
            System.out.println("Invalid input: '" + playerInput + "' is not an integer");
            return;
        }

        if (playerChoice == 1) {
            if (amulet.equals(TRIANGLE_AMULET)) {
                if (weapon.equals(SWORD)) {
                    System.out.println("The amulet pulses and imbues power into your sword,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with power, but doesn't seem to effect your staff!");
                }
            }
            else if (amulet.equals(STAR_AMULET)) {
                if (weapon.equals(STAFF)) {
                    System.out.println("The amulet pulses and strengthens your magic staff,");
                    System.out.println("raising the chance of critical hits.");
                    criticalChance = 75;
                }
                else {
                    System.out.println("The amulet glows with magic, but doesn't seem to effect your sword!");
                }
            }
            else if (amulet.equals(HEART_AMULET)) {
                System.out.println("The amulet glows and pulls energy from the dragon into itself.");
                System.out.println("The dragon lost 10hp!");
                bossHp = bossHp - 10;
            }
            else if (amulet.equals(EYE_AMULET)) {
                System.out.println("The amulet scans the dragon and reveals its hp to you.");
                System.out.println("The dragon has " + bossHp + "hp left.");
            }
        }
        else if (playerChoice == 2) {
            int damage = baseDamage;
            // Roll a random number on [1, 100]
            Random rng = new Random();
            int roll = rng.nextInt(100) + 1;
            if (roll <= criticalChance) {
                damage = criticalDamage;
            }

            System.out.println("You attack the dragon and deal " + damage + "hp of damage!");
            bossHp = bossHp - damage;
        }
        else {
            System.out.println("Invalid choice: '" + playerChoice + "' is out of range");
            return;
        }

        if (bossHp <= 0) {
            System.out.println();
            System.out.println("The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!");
            System.out.println("This will fund your adventuring for years to come!");
            return;
        }

        System.out.println();
        System.out.println("Your luck finally runs out as the dragon connects a strike.");
        System.out.println("You're winded by the massive blow and are unable to dodge");
        System.out.println("the torrent of flames that fall on you, burning you to a crisp.");
    }
}
