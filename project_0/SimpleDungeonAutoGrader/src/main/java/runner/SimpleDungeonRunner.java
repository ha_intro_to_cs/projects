package runner;

import java.io.*;
import java.util.List;

public class SimpleDungeonRunner {
    private File workingDirectory;
    private List<String> runCommand;
    private Process process;

    public SimpleDungeonRunner(File workingDirectory, List<String> runCommand) {
        this.workingDirectory = workingDirectory;
        this.runCommand = runCommand;
    }

    public String launch() throws IOException {
        terminate();
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(runCommand);
        processBuilder.directory(workingDirectory);
        process = processBuilder.start();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        int characterCode;
        while ((characterCode = reader.read()) != -1) {
            char character = (char) characterCode;
            // funny, this is exactly the kind of bug I tell my students not to write...
            if (character == '>') {
                break;
            }
            builder.append(character);
        }

        return builder.toString().trim();
    }

    public String interact(String input) {
        BufferedWriter writer =
                new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
        try {
            writer.write(input);
            writer.newLine();
            writer.flush();
        }
        catch (IOException e) {
            return "__PROGRAM_TERMINATED__";
        }

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        int characterCode;
        try {
            while ((characterCode = reader.read()) != -1) {
                char character = (char) characterCode;
                // funny, this is exactly the kind of bug I tell my students not to write...
                if (character == '>') {
                    break;
                }
                builder.append(character);
            }
        }
        catch (IOException e) {
            return "__PROGRAM_TERMINATED__";
        }

        return builder.toString().trim();
    }

    public void terminate() {
        if (process != null) {
            process.destroy();
        }
    }
}
