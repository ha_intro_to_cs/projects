package grader;

import com.fasterxml.jackson.databind.ObjectMapper;
import compiler.SimpleDungeonCompiler;
import runner.SimpleDungeonRunner;
import solution_builder.DungeonPathData;
import solution_builder.DungeonSolution;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimpleDungeonGrader {
    private static final String usage = "Usage: java SimpleDungeonGrader <project_directory>";

    private static void log(String text) throws IOException {
        System.out.print(text);
        logToFile(text);
    }

    private static void logln(String text) throws IOException {
        log(text + System.lineSeparator());
    }

    private static void logToFile(String text) throws IOException {
        Path logPath = Paths.get("grade.txt");
        BufferedWriter writer = Files.newBufferedWriter(logPath, StandardOpenOption.APPEND);
        writer.write(text);
        writer.flush();
    }

    private static void initLogToFile() throws IOException {
        Path logPath = Paths.get("grade.txt");
        BufferedWriter writer = Files.newBufferedWriter(logPath);
    }

    private static void loglnToFile(String text) throws IOException {
        logToFile(text + System.lineSeparator());
    }

    private static String firstTwoLines(String text) {
        String[] lines = text.split("\n");
        if (lines.length < 2) {
            return "__TOO_FEW_LINES__";
        }
        return lines[0] + System.lineSeparator() + lines[1];
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println(usage);
            return;
        }

        File projectDirectory = Paths.get(args[0]).toFile();

        initLogToFile();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        logln("Test beginning at " + now);

        logln("Compiling in " + projectDirectory);

        SimpleDungeonCompiler.Result compileResult;
        try {
            compileResult = SimpleDungeonCompiler.compile(
                    projectDirectory,
                    Arrays.asList("javac", "SimpleDungeon.java"));
        }
        catch (IOException e) {
            logln("Failed to compile with the following exception:");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            logln(sStackTrace);
            return;
        }

        if (compileResult.success) {
            logln("Compilation successful");
        }
        else {
            logln("Compilation failed with output:");
            logln(compileResult.output);
            return;
        }

        logln("Reading solution");

        ObjectMapper mapper = new ObjectMapper();
        DungeonSolution solution;
        InputStream solutionStream = SimpleDungeonGrader.class.getResourceAsStream("/dungeon_paths_solution.json");
        solution = mapper.readValue(solutionStream, DungeonSolution.class);

        logln("Finished reading solution");

        SimpleDungeonRunner runner = new SimpleDungeonRunner(
                projectDirectory,
                Arrays.asList("java", "SimpleDungeon"));

        logln("Beginning tests");

        int tests = 0;
        int passes = 0;

        String indentation = "    ";
        logln("    Paths");

        int pathsTestIndex = 0;
        for (DungeonPathData pathData : solution.paths) {
            String result;
            result = runner.launch();
            for (String decision : pathData.path) {
                result = runner.interact(decision);
            }
            log("        " + pathsTestIndex + ": ");
            if (result.equals(pathData.data)) {
                passes++;
                logln("PASS");
            }
            else {
                logln("FAIL");
                loglnToFile("######Input");
                for (String decision : pathData.path) {
                    logToFile(decision + ", ");
                }
                loglnToFile("");
                loglnToFile("######Expected");
                loglnToFile(pathData.data);
                loglnToFile("######Actual");
                loglnToFile(result);
            }
            runner.terminate();
            tests++;
            pathsTestIndex++;
        }

        logln("    Parsing");

        log("        NaN: ");
        List<String> parseErrorNanDecisions = Arrays.asList("1", "1", "garbo");
        String nanText = "Invalid input: 'garbo' is not an integer";
        boolean nanPass = true;
        String result = runner.launch();
        for (String decision : parseErrorNanDecisions) {
            result = runner.interact(decision);
        }
        if (!result.equals(nanText)) {
            nanPass = false;
        }
        if (!runner.interact("terminate").equals("__PROGRAM_TERMINATED__")) {
            nanPass = false;
        }

        if (nanPass) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Input");
            for (String decision : parseErrorNanDecisions) {
                logToFile(decision + ", ");
            }
            loglnToFile("");
            loglnToFile("######Expected");
            loglnToFile(nanText);
            loglnToFile("######Actual");
            loglnToFile(result);
        }
        tests++;

        log ("        OOR: ");
        List<String> parseErrorOorDecisions = Arrays.asList("1", "1", "-1");
        String oorText = "Invalid choice: '-1' is out of range";
        boolean oorPass = true;
        result = runner.launch();
        for (String decision : parseErrorOorDecisions) {
            result = runner.interact(decision);
        }
        if (!result.equals(oorText)) {
            oorPass = false;
        }
        if (!runner.interact("terminate").equals("__PROGRAM_TERMINATED__")) {
            oorPass = false;
        }

        if (oorPass) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Input");
            for (String decision : parseErrorOorDecisions) {
                logToFile(decision + ", ");
            }
            loglnToFile("");
            loglnToFile("######Expected");
            loglnToFile(oorText);
            loglnToFile("######Actual");
            loglnToFile(result);
        }
        tests++;

        // What follows is real sloppy. I'm sorry if you have to read this. I ran out of time.

        logln("    Boss");
        logln("        Combat");

        List<String> combatDecisions = Arrays.asList("1", "1", "2", "1", "2", "2", "2");

        log("            Win: ");
        String winText = "The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!\n" +
                "This will fund your adventuring for years to come!";
        boolean winPass = false;
        for (int attempts = 0; attempts < 10; attempts++) {
            //String result;
            result = runner.launch();
            for (String decision : combatDecisions) {
                result = runner.interact(decision);
            }
            if (result.contains(winText)) {
                winPass = true;
                break;
            }
        }
        if (winPass) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Failed to win in 10 attempts, or your win text isn't correct");
        }
        tests++;

        log("            Lose: ");
        String loseText = "Your luck finally runs out as the dragon connects a strike.\n" +
                "You're winded by the massive blow and are unable to dodge\n" +
                "the torrent of flames that fall on you, burning you to a crisp.";
        boolean losePass = false;
        for (int attempts = 0; attempts < 10; attempts++) {
            //String result;
            result = runner.launch();
            for (String decision : combatDecisions) {
                result = runner.interact(decision);
            }
            if (result.contains(loseText)) {
                losePass = true;
                break;
            }
        }
        if (losePass) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Failed to lose in 10 attempts, or your lose text isn't correct");
        }
        tests++;

        log("        Critical: ");

        List<String> oneHitDecisions = Arrays.asList("1", "1", "2", "1", "2");
        String normalHitText = "You attack the dragon and deal 5hp of damage!";
        String criticalHitText = "You attack the dragon and deal 15hp of damage!";
        double normalHits = 0;
        double criticalHits = 0;
        boolean errorHit = false;
        for (int attempts = 0; attempts < 50; attempts++) {
            //String result;
            result = runner.launch();
            for (String decision : oneHitDecisions) {
                result = runner.interact(decision);
            }
            result = result.split(System.lineSeparator())[0];
            if (result.equals(normalHitText)) {
                normalHits++;
            }
            else if (result.equals(criticalHitText)) {
                criticalHits++;
            }
            else {
                errorHit = true;
                break;
            }
        }
        if (errorHit) {
            logln("FAIL");
            loglnToFile("######Invalid hit text");
        }
        else {
            double experimentalCriticalPercent = (criticalHits) / (criticalHits + normalHits);
            if (experimentalCriticalPercent < 0.35 && experimentalCriticalPercent > 0.15) {
                logln("PASS (" + experimentalCriticalPercent * 100 + "%)");
                passes++;
            }
            else {
                logln("FAIL (" + experimentalCriticalPercent * 100 + "%)");
                loglnToFile("######Experimentally determined critical chance over 50 runs is out of range");
            }
        }
        tests++;

        logln("        Amulets");

        log("            Triangle: ");
        List<String> triangleHitDecisions = Arrays.asList("1", "1", "2", "1", "1", "2");
        normalHits = 0;
        criticalHits = 0;
        for (int attempts = 0; attempts < 50; attempts++) {
            //String result;
            result = runner.launch();
            for (String decision : triangleHitDecisions) {
                result = runner.interact(decision);
            }
            result = result.split(System.lineSeparator())[0];
            if (result.equals(normalHitText)) {
                normalHits++;
            }
            else {
                criticalHits++;
            }
        }
        double experimentalCriticalPercent = (criticalHits) / (criticalHits + normalHits);
        if (experimentalCriticalPercent < 0.85 && experimentalCriticalPercent > 0.65) {
            logln("PASS (" + experimentalCriticalPercent * 100 + "%)");
            passes++;
        }
        else {
            logln("FAIL (" + experimentalCriticalPercent * 100 + "%)");
            loglnToFile("######Experimentally determined critical chance over 50 runs is out of range");
        }
        tests++;

        log("            Triangle no effect: ");
        List<String> triangleNoEffectDecisions = Arrays.asList("2", "1", "1", "1", "1");
        String triangleNoEffectText = "The amulet glows with power, but doesn't seem to effect your staff!";
        result = runner.launch();
        for (String decision : triangleNoEffectDecisions) {
            result = runner.interact(decision);
        }
        if (result.contains(triangleNoEffectText)) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Input");
            for (String decision :triangleNoEffectDecisions) {
                logToFile(decision + ", ");
            }
            loglnToFile("");
            loglnToFile("######Expected");
            loglnToFile(triangleNoEffectText);
            loglnToFile("######Actual");
            loglnToFile(result);
        }
        tests++;

        log("            Star: ");
        List<String> starHitDecisions = Arrays.asList("2", "1", "1", "2", "1", "2");
        normalHits = 0;
        criticalHits = 0;
        for (int attempts = 0; attempts < 50; attempts++) {
            //String result;
            result = runner.launch();
            for (String decision : starHitDecisions) {
                result = runner.interact(decision);
            }
            result = result.split(System.lineSeparator())[0];
            if (result.equals(normalHitText)) {
                normalHits++;
            }
            else {
                criticalHits++;
            }
        }
        experimentalCriticalPercent = (criticalHits) / (criticalHits + normalHits);
        if (experimentalCriticalPercent < 0.85 && experimentalCriticalPercent > 0.65) {
            logln("PASS (" + experimentalCriticalPercent * 100 + "%)");
            passes++;
        }
        else {
            logln("FAIL (" + experimentalCriticalPercent * 100 + "%)");
            loglnToFile("######Experimentally determined critical chance over 50 runs is out of range");
        }
        tests++;

        log("            Star no effect: ");
        List<String> starNoEffectDecisions = Arrays.asList("1", "1", "2", "2", "1");
        String starNoEffectText = "The amulet glows with magic, but doesn't seem to effect your sword!";
        result = runner.launch();
        for (String decision : starNoEffectDecisions) {
            result = runner.interact(decision);
        }
        if (result.contains(starNoEffectText)) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Input");
            for (String decision :starNoEffectDecisions) {
                logToFile(decision + ", ");
            }
            loglnToFile("");
            loglnToFile("######Expected");
            loglnToFile(starNoEffectText);
            loglnToFile("######Actual");
            loglnToFile(result);
        }
        tests++;

        log("            Heart: ");
        List<String> heartDecisions = Arrays.asList("1", "2", "fire", "1");
        String heartText1 = "The amulet glows and pulls energy from the dragon into itself.\n" +
                "The dragon lost 10hp!";
        String heartText2 = "The amulet glows and pulls energy from the dragon into itself.\n" +
                "The dragon lost 10hp!\n" +
                "\n" +
                "The dragon roars as it crumbles in defeat. Its entire hoard belongs to you!\n" +
                "This will fund your adventuring for years to come!";
        //String result;
        boolean heartPass = true;
        result = runner.launch();
        for (String decision : heartDecisions) {
            result = runner.interact(decision);
        }
        result = runner.interact("1");
        result = firstTwoLines(result);
        if (!result.equals(heartText1)) {
            heartPass = false;
        }
        result = runner.interact("1");
        if (!result.equals(heartText2)) {
            heartPass = false;
        }
        if (heartPass) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Heart Amulet x2 did not kill the dragon. Last output: " + result);
        }
        tests++;

        log("            Eye: ");
        List<String> eyeDecisions = Arrays.asList("1", "2", "fire", "2");
        String eyeText = "The amulet scans the dragon and reveals its hp to you.\n" +
                "The dragon has 20hp left.";
        result = runner.launch();
        for (String decision : eyeDecisions) {
            result = runner.interact(decision);
        }
        result = runner.interact("1");
        result = firstTwoLines(result);
        if (result.equals(eyeText)) {
            logln("PASS");
            passes++;
        }
        else {
            logln("FAIL");
            loglnToFile("######Expected");
            loglnToFile(eyeText);
            loglnToFile("######Actual");
            loglnToFile(result);
        }
        tests++;

        logln("");
        logln("Total: " + passes + "/" + tests);
    }
}
