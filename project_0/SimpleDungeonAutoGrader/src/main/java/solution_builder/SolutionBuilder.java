package solution_builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import compiler.SimpleDungeonCompiler;
import runner.SimpleDungeonRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class SolutionBuilder {
    public static void main(String[] args) throws IOException {

        File projectDirectory = Paths.get(args[0]).toFile();

        SimpleDungeonCompiler.Result compileResult = SimpleDungeonCompiler.compile(
                projectDirectory,
                Arrays.asList("javac", "SimpleDungeon.java"));
        if (!compileResult.success) {
            System.out.println("Failed to compile!");
            System.out.println(compileResult.output);
        }

        ObjectMapper mapper = new ObjectMapper();
        DungeonPathsJson json = mapper.readValue(Paths.get("dungeon_paths.json").toFile(), DungeonPathsJson.class);

        DungeonSolution solution = new DungeonSolution();
        for(List<String> path : json.paths) {
            solution.paths.add(new DungeonPathData(path, ""));
        }

        List<String> runCommand = Arrays.asList("java", "SimpleDungeon");
        SimpleDungeonRunner runner = new SimpleDungeonRunner(projectDirectory, runCommand);

        for (DungeonPathData pathData : solution.paths) {
            String result;
            result = runner.launch();
            for (String decision : pathData.path) {
                result = runner.interact(decision);
            }
            pathData.data = result;
            runner.terminate();
        }

        File outputFile = Paths.get("dungeon_paths_solution.json").toFile();
        mapper.writeValue(outputFile, solution);
    }
}
