package solution_builder;

import java.util.ArrayList;
import java.util.List;

public class DungeonPathData {
    public List<String> path = new ArrayList<>();
    public String data;
    public DungeonPathData(List<String> path, String data) {
        this.path = path;
        this.data = data;
    }

    public DungeonPathData() {

    }
}
